import React, { useState } from "react";
import ContactList from "../components/ContactList/ContactList";
import SearchInput from "../components/SearchInput/SearchInput";
import TopBar from "../components/TopBar/TopBar";
import styles from "../styles/Contacts.module.scss";

export default function Contacts() {
  const [inputValue, setInputValue] = useState("");

  const getInputValue = (dataFromChild) => {
    setInputValue(dataFromChild);
  };

  return (
    <div className={styles.contacts}>
      <TopBar />
      <SearchInput getInputValue={getInputValue} />
      <ContactList inputValue={inputValue} />
    </div>
  );
}
