import React from "react";

import TextField from "@material-ui/core/TextField";
import SearchIcon from "@material-ui/icons/Search";
import InputAdornment from "@material-ui/core/InputAdornment";

const SearchInput = ({ getInputValue }) => {
  const handleChange = (e) => {
    getInputValue(e.target.value);
  };

  return (
    <TextField
      onChange={handleChange}
      InputProps={{
        startAdornment: (
          <InputAdornment position="start">
            <SearchIcon color="disabled" />
          </InputAdornment>
        ),
      }}
      fullWidth
    />
  );
};

export default SearchInput;
