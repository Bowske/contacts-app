import React, { useState, useEffect } from "react";
import ContactTile from "../ContactTile/ContactTile";
import variables from "./../../variables/variables";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TablePagination from "@material-ui/core/TablePagination";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import styles from "./ContactList.module.scss";

const ContactList = ({ inputValue }) => {
  const [contacts, setContacts] = useState([]);
  const [loading, setLoading] = useState(true);
  const [listOfChecked, setListOfChecked] = useState([]);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [page, setPage] = useState(0);

  useEffect(() => {
    fetch(variables.LINK)
      .then((res) => res.json())
      .then((data) =>
        data.sort((a, b) => a.last_name.localeCompare(b.last_name))
      )
      .then((sortedData) => {
        setContacts(sortedData);
        setLoading(false);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  useEffect(() => {
    console.log(listOfChecked);
  }, [listOfChecked]);

  const filteredContacts = contacts.filter((contact) =>
    `${contact.first_name} ${contact.last_name}`
      .toLowerCase()
      .includes(inputValue.toLowerCase())
  );
  const addToChecked = (id) => {
    setListOfChecked([...listOfChecked, id]);
  };
  const removeFromChecked = (id) => {
    const newList = listOfChecked.filter((i) => i != id);
    setListOfChecked(newList);
  };
  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };
  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  return (
    <div>
      {!loading ? (
        <Paper className={styles.contactList}>
          <TableContainer component={Paper}>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell align="left">Avatar</TableCell>
                  <TableCell align="right">First name</TableCell>
                  <TableCell align="right">Last name</TableCell>
                  <TableCell align="right">Email</TableCell>
                  <TableCell align="right">Checkbox</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {filteredContacts
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((contact) => (
                    <ContactTile
                      page={page}
                      list={listOfChecked}
                      addToChecked={addToChecked}
                      removeFromChecked={removeFromChecked}
                      key={contact.id}
                      id={contact.id}
                      avatar={contact.avatar}
                      firstName={contact.first_name}
                      lastName={contact.last_name}
                      email={contact.email}
                    />
                  ))}
              </TableBody>
            </Table>
          </TableContainer>
          <TablePagination
            rowsPerPageOptions={[5, 10, 25]}
            component="div"
            count={filteredContacts.length}
            rowsPerPage={rowsPerPage}
            page={page}
            onChangePage={handleChangePage}
            onChangeRowsPerPage={handleChangeRowsPerPage}
          />
        </Paper>
      ) : (
        <span>{variables.LOADING_SCREEN}</span>
      )}
    </div>
  );
};

export default ContactList;
