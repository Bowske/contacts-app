import React, { useState, useEffect } from "react";
import Avatar from "@material-ui/core/Avatar";
import Checkbox from "@material-ui/core/Checkbox";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";

const ContactTile = ({
  id,
  firstName,
  lastName,
  email,
  avatar,
  addToChecked,
  removeFromChecked,
  logCheckedList,
  page,
  list,
}) => {
  const [isChecked, setIsChecked] = useState(false);
  useEffect(() => {
    if (list.includes(id)) {
      setIsChecked(true);
    }
  }, [page]);

  const handleChange = (event) => {
    const check = !isChecked;
    check ? addToChecked(id) : removeFromChecked(id);
    setIsChecked(check);
  };

  return (
    <TableRow key={id}>
      <TableCell align="right">
        {avatar ? (
          <Avatar src={avatar} />
        ) : (
          <Avatar>{firstName.charAt(0) + lastName.charAt(0)}</Avatar>
        )}
      </TableCell>
      <TableCell align="right">{firstName}</TableCell>
      <TableCell align="right">{lastName}</TableCell>
      <TableCell align="right">{email}</TableCell>
      <TableCell align="right">
        <Checkbox
          checked={isChecked}
          color="primary"
          onClick={logCheckedList}
          onChange={handleChange}
        />
      </TableCell>
    </TableRow>
  );
};

export default ContactTile;
