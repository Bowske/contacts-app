import React from "react";
import variables from "../../variables/variables";
import styles from "./TopBar.module.scss";

const TopBar = () => {
  return (
    <div className={styles.topBar}>
      <span className={styles.topBar__text}>{variables.TOPBAR_TITLE}</span>
    </div>
  );
};

export default TopBar;
